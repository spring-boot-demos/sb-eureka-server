package com.example.sbeurekaserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class SbEurekaServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SbEurekaServerApplication.class, args);
	}

}
